$ ("#bekezdes").html("Hello <b> World!</b>").css("color", "blue");

var flag = false;
function buttonClicked() {
	$("#bekezdes").css("color", flag ? "blue" : "red");
	flag =!flag;
}

$('button').on("click", buttonClicked);

$('.faq a.question').on("click", function(e){
	e.preventDefault();
	$('.answer').hide();
	$(this).siblings('.answer').toggle();
});